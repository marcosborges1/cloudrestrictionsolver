/* 
 * AutoRefactor - Eclipse plugin to automatically refactor Java code bases. 
 * 
 * Copyright (C) 2013-2015 Jean-Noël Rouvignac - initial API and implementation 
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program under LICENSE-GNUGPL.  If not, see 
 * <http://www.gnu.org/licenses/>. 
 * 
 * 
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution under LICENSE-ECLIPSE, and is 
 * available at http://www.eclipse.org/legal/epl-v10.html 
 */
package org.autorefactor.refactoring.rules;

import static org.autorefactor.refactoring.ASTHelper.VISIT_SUBTREE;
import static org.autorefactor.refactoring.ASTHelper.arguments;
import static org.autorefactor.refactoring.ASTHelper.as;
import static org.autorefactor.refactoring.ASTHelper.asExpression;
import static org.autorefactor.refactoring.ASTHelper.fragments;
import static org.autorefactor.refactoring.ASTHelper.getPreviousStatement;
import static org.autorefactor.refactoring.ASTHelper.isMethod;
import static org.autorefactor.refactoring.ASTHelper.isNodeTypeClass;
import static org.autorefactor.refactoring.ASTHelper.isSameLocalVariable;
import static org.autorefactor.refactoring.ASTHelper.removeParentheses;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.autorefactor.analyzer.JSONAnalyzer;
import org.autorefactor.refactoring.ASTBuilder;
import org.autorefactor.refactoring.Refactorings;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/** See {@link #getDescription()} method. */
@SuppressWarnings("javadoc")
public class CRS4GAEFileOutputStreamRefactoring extends AbstractRefactoringRule {

	// private String nameViolatedVariables[] = { "out" };
	private String nameViolatedVariables[] = this.readJSONData();
	private String nameClassCRS = "CRS4GAEWritingFiles";
	private List<Statement> nodeLists = new ArrayList<Statement>();
	private String[] classesAttended = { "java.io.OutputStream", "java.io.FileOutputStream" };

	public List<Statement> getNodeLists() {
		return nodeLists;
	}

	public String getNameClassCRS() {
		return nameClassCRS;
	}

	public void setNameClassCRS(String nameClassCRS) {
		this.nameClassCRS = nameClassCRS;
	}

	@Override
	public String getDescription() {
		return "Refactor files that have restriction on a cloud with equivalent Collections APIs.";
	}

	@Override
	public String getName() {
		return "CRS4GAEFileOutputStreamRefactoring";
	}

	@Override
	public boolean visit(ExpressionStatement node) {

		ASTBuilder builder = this.ctx.getASTBuilder();
		Refactorings refactoring = this.ctx.getRefactorings();

		// Obter todas as expressões como método
		final MethodInvocation mi = asExpression(node, MethodInvocation.class);

		for (String classAttend : classesAttended) {
			// Verifica se são as mesmas variáveis apontadas na identificação
			if (mi != null && mi.getExpression() != null) {
				if (this.checkViolatedVariables(mi.getExpression().toString())) {
					if (isMethod(mi, classAttend, "write", "byte[]", "int", "int")) {
						if (mi.getParent().getParent().getParent().getNodeType() == ASTNode.WHILE_STATEMENT) {
							Expression argument1 = arguments(mi).get(0);
							Expression argument3 = arguments(mi).get(2);
							WhileStatement whi = (WhileStatement) mi.getParent().getParent().getParent();
							updateArgumentOfStatements(argument1, getAllStatementsBeforeNode(node),
									getParameterWhile(whi));
							removeArgumentOfStatements(argument3, getAllStatementsBeforeNode(node));
							refactoring.replace(whi, builder.toStmt(builder
									.createNewMethodStatic(this.getNameClassCRS(), "write", builder.copy(argument1))));
						}
					}
					if (isMethod(mi, classAttend, "flush")) {
						refactoring.remove(mi.getParent());
					}
					if (isMethod(mi, classAttend, "close")) {
						refactoring.replace(mi.getParent(),
								builder.toStmt(builder.createNewMethodStatic(this.getNameClassCRS(), "close")));
					}
				}
			}

			// Obter todas os expressões como assignments
			final Assignment assignments = asExpression(node, Assignment.class);
			if (assignments != null && this.checkViolatedVariables(assignments.getLeftHandSide().toString())) {
				if (classAttend.equals(assignments.resolveTypeBinding().getQualifiedName())) {
					this.ctx.getRefactorings().replace(assignments, builder.createNewMethodStatic(
							this.getNameClassCRS(), "setOutputChannel", builder.copy(assignments.getRightHandSide())));
				}
			}
		}

		return true;
	}

	public Expression getParameterWhile(WhileStatement node) {
		// Se o nó é infix
		if (node.getExpression().getNodeType() == ASTNode.INFIX_EXPRESSION) {
			// Faz o Casting
			InfixExpression e = (InfixExpression) node.getExpression();
			// Remove os parenteses
			Assignment a = (Assignment) removeParentheses(e.getLeftOperand());
			if (a.getRightHandSide().getNodeType() == ASTNode.METHOD_INVOCATION) {
				MethodInvocation mi = (MethodInvocation) a.getRightHandSide();
				// Nome do variável que chama o método
				return mi.getExpression();
			}
		}
		return null;
	}

	private void updateArgumentOfStatements(Expression argument, List<Statement> allStatementsBeforeNode,
			Expression name) {
		// TODO Auto-generated method stub
		ASTBuilder builder = this.ctx.getASTBuilder();

		for (Statement statement : allStatementsBeforeNode) {
			if (statement instanceof VariableDeclarationStatement) {
				VariableDeclarationStatement var = as(statement, VariableDeclarationStatement.class);
				final VariableDeclarationFragment vdf = fragments(var).get(0);
				if (isSameLocalVariable(argument, vdf.resolveBinding())) {
					this.ctx.getRefactorings().replace(vdf.getInitializer(), builder.createNewMethodStatic(
							this.getNameClassCRS(), "convertFileToByteArray", builder.copy(name)));
				}
			}
		}

	}

	private void removeArgumentOfStatements(Expression argument, List<Statement> allStatementsBeforeNode) {
		// TODO Auto-generated method stub
		for (Statement statement : allStatementsBeforeNode) {
			if (statement instanceof VariableDeclarationStatement) {
				VariableDeclarationStatement var = as(statement, VariableDeclarationStatement.class);
				final VariableDeclarationFragment vdf = fragments(var).get(0);
				if (isSameLocalVariable(argument, vdf.resolveBinding())) {
					this.ctx.getRefactorings().remove(var);
				}
			}
		}
		// Olhar como foi feito no Mab.
	}

	public List<Statement> getAllStatementsBeforeNode(Statement node) {

		if (getPreviousStatement(node) != null) {
			nodeLists.add(getPreviousStatement(node));
			getAllStatementsBeforeNode(getPreviousStatement(node));
		}
		return getNodeLists();
	}

	@Override
	public boolean visit(ClassInstanceCreation node) {

		final ITypeBinding typeBinding = node.getType().resolveBinding();

		// Pega os argumentos e transforma em expressão.
		final List<Expression> args = arguments(node);

		// ASTBuilder classes que mexe diretamento na árvore
		final ASTBuilder builder = this.ctx.getASTBuilder();

		// Classes instanciadas que tem só um parâmetro. Ex: new File(parametro)
		if (typeBinding != null && args.size() == 1) {
			// Nome da classe, equivalente ao import.
			final String qualifiedName = typeBinding.getQualifiedName();

			if ("java.io.File".equals(qualifiedName)) {
				// Se o tipo do nó é uma classe.Importante: toda vez que eu
				// quiser saber as informações de um nó específico, eu tenho que
				// saber o tipo de nó dele.
				if (isNodeTypeClass(node.getParent().getNodeType())) {
					// Fazer o casting com o nó específico
					ClassInstanceCreation classInstance = (ClassInstanceCreation) node.getParent();
					if ("java.io.FileOutputStream"
							.equals(classInstance.getType().resolveBinding().getQualifiedName())) {

						// Método que susbstitui um nó.
						this.ctx.getRefactorings().replace(node, builder.createNewMethodStatic(this.getNameClassCRS(),
								"newCloudFile", builder.copy(args.get(0))));
						node.getParent().accept(this);
					}
				}

			}
			if ("java.io.FileOutputStream".equals(qualifiedName)) {
				if (arguments(node).get(0).getNodeType() == ASTNode.METHOD_INVOCATION) {
					this.ctx.getRefactorings().replace(node,
							builder.createNewMethodStatic(this.getNameClassCRS(), "create", builder.copy(args.get(0))));
				}
			}
		}
		return VISIT_SUBTREE; // Retornar na árvore
	}

	@Override
	public boolean visit(VariableDeclarationFragment node) {
		final ASTBuilder builder = this.ctx.getASTBuilder();
		for (String classAttend : classesAttended) {

			if (classAttend.equals(node.resolveBinding().getType().getQualifiedName())
					&& node.getInitializer().getNodeType() == ASTNode.METHOD_INVOCATION) {
				// toStatement - criar statements com ;
				this.ctx.getRefactorings().replace(node.getParent(),
						this.ctx.getASTBuilder().toStmt(builder.createNewMethodStatic(this.getNameClassCRS(),
								"setOutputChannel", builder.copy(node.getInitializer()))));
			}
			// System.err.println(node);
		}
		return VISIT_SUBTREE;
	}

	// private Expression createNewMethodStatic(String newClass, String
	// nameMethod, Expression... args) {
	// return
	// this.ctx.getASTBuilder().invoke(this.ctx.getAST().newSimpleName(newClass),
	// nameMethod, args);
	// }

	public boolean checkViolatedVariables(String name) {
		for (String nameCurrent : nameViolatedVariables) {
			if (name.equals(nameCurrent))
				return true;
		}
		return false;
	}

	public String[] readJSONData() {

		JSONParser parser = new JSONParser();
		String[] arrayString = null;
		try {
			String currentFile = System.getProperty("user.home") + "/crsanalyzer/UploadFileService.json";
			Object obj = parser.parse(new FileReader(currentFile));
			JSONObject json = (JSONObject) obj;
			JSONArray arrayValues = JSONAnalyzer.getValuesOfArray(json, "declaration", "variable",
					"java.io.OutputStream");
			arrayString = JSONAnalyzer.jsonArrayToString(arrayValues);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return arrayString;
	}

}