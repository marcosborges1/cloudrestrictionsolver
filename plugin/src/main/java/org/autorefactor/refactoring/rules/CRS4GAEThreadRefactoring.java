/* 
 * AutoRefactor - Eclipse plugin to automatically refactor Java code bases. 
 * 
 * Copyright (C) 2013-2015 Jean-Noël Rouvignac - initial API and implementation 
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program under LICENSE-GNUGPL.  If not, see 
 * <http://www.gnu.org/licenses/>. 
 * 
 * 
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution under LICENSE-ECLIPSE, and is 
 * available at http://www.eclipse.org/legal/epl-v10.html 
 */
package org.autorefactor.refactoring.rules;

import static org.autorefactor.refactoring.ASTHelper.VISIT_SUBTREE;
import static org.autorefactor.refactoring.ASTHelper.arguments;
import static org.autorefactor.refactoring.ASTHelper.asExpression;
import static org.autorefactor.refactoring.ASTHelper.getPreviousStatement;
import static org.autorefactor.refactoring.ASTHelper.isMethod;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.autorefactor.analyzer.CRSConsole;
import org.autorefactor.analyzer.JSONAnalyzer;
import org.autorefactor.refactoring.ASTBuilder;
import org.autorefactor.refactoring.Refactorings;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/** See {@link #getDescription()} method. */
@SuppressWarnings("javadoc")
public class CRS4GAEThreadRefactoring extends AbstractRefactoringRule {
	
//	private String nameViolatedVariables[] = { "searcher" };
	private String nameClassCRS = "CRS4GAEThread";
	private String[] nameViolatedVariables = this.visitJSON();
	private List<Statement> nodeLists = new ArrayList<Statement>();
	ASTBuilder bu;
	
	public CRS4GAEThreadRefactoring() {
	System.err.println("asd");
	}

	public List<Statement> getNodeLists() {
		return nodeLists;
	}

	public String getNameClassCRS() {
		return nameClassCRS;
	}

	public void setNameClassCRS(String nameClassCRS) {
		this.nameClassCRS = nameClassCRS;
	}

	@Override
	public String getDescription() {
		return "Refactor threads that have restriction on a cloud with equivalent Collections APIs.";
	}

	@Override
	public String getName() {
		return "CRS4GAEThreadRefactoring";
	}

	@Override
	public boolean visit(ExpressionStatement node) {

		ASTBuilder builder = this.ctx.getASTBuilder();
		Refactorings refactoring = this.ctx.getRefactorings();

		// Obter todas as expressões como método
		final MethodInvocation mi = asExpression(node, MethodInvocation.class);
		// Verifica se são as mesmas variáveis apontadas na identificação
		if (mi != null && this.checkViolatedVariables(mi.getExpression().toString())) {
			if (isMethod(mi, "java.lang.Thread", "setPriority", "int")) {
				refactoring.remove(mi.getParent());
			}
		}

		return VISIT_SUBTREE;
	}

	public List<Statement> getAllStatementsBeforeNode(Statement node) {

		if (getPreviousStatement(node) != null) {
			nodeLists.add(getPreviousStatement(node));
			getAllStatementsBeforeNode(getPreviousStatement(node));
		}
		return getNodeLists();
	}

	@Override
	public boolean visit(ClassInstanceCreation node) {

		final ITypeBinding typeBinding = node.getType().resolveBinding();
		// Pega os argumentos etransforma em expressão.
		final List<Expression> args = arguments(node);
		// ASTBuilder classes que mexe diretamento na árvore
		final ASTBuilder builder = this.ctx.getASTBuilder();

		/*
		 * Classes instanciadas que tem só um parâmetro. Ex: new File(parametro)
		 */
		if (typeBinding != null && args.size() == 1) {

			/* Nome da classe, equivalente ao import. */
			final String qualifiedName = typeBinding.getQualifiedName();
			if ("java.lang.Thread".equals(qualifiedName)) {

				this.ctx.getRefactorings().replace(node, builder.createNewMethodStatic(this.getNameClassCRS(),
						"newCloudThread", builder.copy(args.get(0))));
			}

		}
		return VISIT_SUBTREE; // Retornar na árvore
	}

	// private Expression createNewMethodStatic(String newClass, String
	// nameMethod, Expression... args) {
	// return
	// this.ctx.getASTBuilder().invoke(this.ctx.getAST().newSimpleName(newClass),
	// nameMethod, args);
	// }

	public boolean checkViolatedVariables(String name) {
		for (String nameCurrent : nameViolatedVariables) {
			if (name.equals(nameCurrent))
				return true;
		}
		return false;
	}

	// Teste

	public String[] visitJSON() {
		JSONParser parser = new JSONParser();
		String[] y = null;
		try {
			String currentFile = System.getProperty("user.home") + "/crsanalyzer/MyServlet.json";
//
			Object obj = parser.parse(new FileReader(currentFile));
			JSONObject json = (JSONObject) obj;
//			JSONObject jsonAux = (JSONObject) json.get("declaration");
//			JSONObject jsonAux2 = (JSONObject) jsonAux.get("variable");
//			JSONArray arrayVariable = (JSONArray) jsonAux2.get("FileOutputStream");
//			for (Object object : arrayVariable) {
//				System.err.println(object);
//			}
			JSONArray x = JSONAnalyzer.getValuesOfArray(json, "declaration","variable","java.lang.Thread");
			y = JSONAnalyzer.jsonArrayToString(x);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return y;
	}

}