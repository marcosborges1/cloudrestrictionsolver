/* 
 * AutoRefactor - Eclipse plugin to automatically refactor Java code bases. 
 * 
 * Copyright (C) 2013-2015 Jean-Noël Rouvignac - initial API and implementation 
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program under LICENSE-GNUGPL.  If not, see 
 * <http://www.gnu.org/licenses/>. 
 * 
 * 
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution under LICENSE-ECLIPSE, and is 
 * available at http://www.eclipse.org/legal/epl-v10.html 
 */
package org.autorefactor.refactoring.rules;

import static org.autorefactor.refactoring.ASTHelper.VISIT_SUBTREE;
import static org.autorefactor.refactoring.ASTHelper.arguments;
import static org.autorefactor.refactoring.ASTHelper.as;
import static org.autorefactor.refactoring.ASTHelper.asExpression;
import static org.autorefactor.refactoring.ASTHelper.fragments;
import static org.autorefactor.refactoring.ASTHelper.getPreviousStatement;
import static org.autorefactor.refactoring.ASTHelper.isMethod;
import static org.autorefactor.refactoring.ASTHelper.isNodeTypeClass;
import static org.autorefactor.refactoring.ASTHelper.isSameLocalVariable;
import static org.autorefactor.refactoring.ASTHelper.removeParentheses;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.autorefactor.analyzer.JSONAnalyzer;
import org.autorefactor.refactoring.ASTBuilder;
import org.autorefactor.refactoring.ASTHelper;
import org.autorefactor.refactoring.Refactorings;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.ExpressionStatement;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Statement;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.eclipse.jdt.core.dom.WhileStatement;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/** See {@link #getDescription()} method. */
@SuppressWarnings("javadoc")
public class CRS4GAEFileOutputStreamRefactoring2 extends AbstractRefactoringRule {

	private String nameViolatedVariables[] = { "out" };
	// private String nameViolatedVariables[] = this.readJSONData();
	private String nameClassCRS = "CRS4GAEWritingFiles";
	private List<Statement> nodeLists = new ArrayList<Statement>();
	private String[] classesAttended = { "java.io.FileOutputStream" };

	public List<Statement> getNodeLists() {
		return nodeLists;
	}

	public String getNameClassCRS() {
		return nameClassCRS;
	}

	public void setNameClassCRS(String nameClassCRS) {
		this.nameClassCRS = nameClassCRS;
	}

	@Override
	public String getDescription() {
		return "Refactor files that have restriction on a cloud with equivalent Collections APIs.";
	}

	@Override
	public String getName() {
		return "CRS4GAEFileOutputStreamRefactoring";
	}

	public String getNameClassComunication() {
		return "CRS4GAEFileOutputStream";
	}

	public String getRestrictedClass() {
		return "java.io.FileOutputStream";
	}

	public String getSuperClassFromRestrictedClass() {
		return "java.io.OutputStream";
	}

	@Override
	public boolean visit(ClassInstanceCreation node) {

		final ITypeBinding typeBinding = node.getType().resolveBinding();
		final List<Expression> arguments = ASTHelper.arguments(node);

		ASTBuilder builder = this.ctx.getASTBuilder();
		Refactorings refactorings = this.ctx.getRefactorings();

		if (typeBinding != null && arguments.size() == 1) {

			final String fullNameNodeClass = typeBinding.getQualifiedName();
			if (getRestrictedClass().equals(fullNameNodeClass)) {
				refactorings.replace(node,
						builder.createNewClass(this.getNameClassComunication(), builder.copy(arguments.get(0))));
			}
		}
		return VISIT_SUBTREE;
	}

	@Override
	public boolean visit(ExpressionStatement node) {

		ASTBuilder builder = this.ctx.getASTBuilder();
		Refactorings refactorings = this.ctx.getRefactorings();
		final MethodInvocation mi = ASTHelper.asExpression(node, MethodInvocation.class);
		if (mi != null && mi.getExpression() != null) {
			String variableName = mi.getExpression().toString();
			if (this.checkViolatedVariables(variableName)) {
				if (ASTHelper.isMethod(mi, this.getRestrictedClass(), "write", "byte[]", "int", "int") || ASTHelper
						.isMethod(mi, this.getSuperClassFromRestrictedClass(), "write", "byte[]", "int", "int")) {
					if (mi.getParent().getParent().getParent().getNodeType() == ASTNode.WHILE_STATEMENT) {
						Expression argument1 = ASTHelper.arguments(mi).get(0);
						Expression argument3 = ASTHelper.arguments(mi).get(2);
						WhileStatement whi = (WhileStatement) mi.getParent().getParent().getParent();
						updateArgumentOfStatements(argument1, getAllStatementsBeforeNode(node), getParameterWhile(whi));
						removeArgumentOfStatements(argument3, getAllStatementsBeforeNode(node));
						refactorings.replace(whi,
								builder.toStmt(builder.invoke(variableName, "write", builder.copy(argument1))));
					}
				}
			}
		}

		return true;
	}

	public Expression getParameterWhile(WhileStatement node) {
		if (node.getExpression().getNodeType() == ASTNode.INFIX_EXPRESSION) {
			InfixExpression e = (InfixExpression) node.getExpression();
			Assignment a = (Assignment) removeParentheses(e.getLeftOperand());
			if (a.getRightHandSide().getNodeType() == ASTNode.METHOD_INVOCATION) {
				MethodInvocation mi = (MethodInvocation) a.getRightHandSide();
				return mi.getExpression();
			}
		}
		return null;
	}

	private void updateArgumentOfStatements(Expression argument, List<Statement> allStatementsBeforeNode,
			Expression name) {

		// Tree-nodes creators (ASTBuilder) and refactorings (Refactorings)
		ASTBuilder builder = this.ctx.getASTBuilder();
		Refactorings refactorings = this.ctx.getRefactorings();

		for (Statement statement : allStatementsBeforeNode) {
			if (statement instanceof VariableDeclarationStatement) {
				VariableDeclarationStatement var = as(statement, VariableDeclarationStatement.class);
				final VariableDeclarationFragment vdf = fragments(var).get(0);
				if (isSameLocalVariable(argument, vdf.resolveBinding())) {
					refactorings.replace(vdf.getInitializer(), builder.createNewMethodStatic(
							super.getClassNameCRSUtils(), "convertFileToByteArray", builder.copy(name)));
				}
			}
		}

	}

	private void removeArgumentOfStatements(Expression argument, List<Statement> allStatementsBeforeNode) {
		for (Statement statement : allStatementsBeforeNode) {
			if (statement instanceof VariableDeclarationStatement) {
				VariableDeclarationStatement var = as(statement, VariableDeclarationStatement.class);
				final VariableDeclarationFragment vdf = fragments(var).get(0);
				if (isSameLocalVariable(argument, vdf.resolveBinding())) {
					this.ctx.getRefactorings().remove(var);
				}
			}
		}
	}

	public List<Statement> getAllStatementsBeforeNode(Statement node) {

		if (getPreviousStatement(node) != null) {
			nodeLists.add(getPreviousStatement(node));
			getAllStatementsBeforeNode(getPreviousStatement(node));
		}
		return getNodeLists();
	}

	public boolean checkViolatedVariables(String name) {
		for (String nameCurrent : nameViolatedVariables) {
			if (name.equals(nameCurrent))
				return true;
		}
		return false;
	}

	public String[] readJSONData() {

		JSONParser parser = new JSONParser();
		String[] arrayString = null;
		try {
			String currentFile = System.getProperty("user.home") + "/crsanalyzer/UploadFileService.json";
			Object obj = parser.parse(new FileReader(currentFile));
			JSONObject json = (JSONObject) obj;
			JSONArray arrayValues = JSONAnalyzer.getValuesOfArray(json, "declaration", "variable",
					"java.io.OutputStream");
			arrayString = JSONAnalyzer.jsonArrayToString(arrayValues);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return arrayString;
	}

}