package org.autorefactor.analyzer;

import java.util.HashMap;
import java.util.List;

public class GAE extends PaaS {

	public GAE() {
		this.abbreviation = "GAE";
		this.description = "Google App Engine";
		listRestrictedClasses = new HashMap<String, List<DetectionTypes>>();
	}

	public void registerRestritedClasses() {
		listRestrictedClasses.put("java.lang.Thread", addDetections(DetectionTypes.instanciation));
		
//		listRestrictedClasses.put("java.lang.Runnable",
//				addDetections(DetectionTypes.implementation));
//		listRestrictedClasses.put("java.io.FileOutputStream",
//				addDetections(DetectionTypes.declaration, DetectionTypes.implementation,DetectionTypes.instanciation));
//		listRestrictedClasses.put("java.io.OutputStream",
//				addDetections(DetectionTypes.declaration, DetectionTypes.implementation,DetectionTypes.instanciation));
//		listRestrictedClasses.put("java.io.File",
//				addDetections(DetectionTypes.declaration, DetectionTypes.implementation,DetectionTypes.instanciation));
	}

}
