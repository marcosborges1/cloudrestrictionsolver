/* 
 * AutoRefactor - Eclipse plugin to automatically refactor Java code bases. 
 * 
 * Copyright (C) 2013-2015 Jean-Noël Rouvignac - initial API and implementation 
 * 
 * This program is free software: you can redistribute it and/or modify 
 * it under the terms of the GNU General Public License as published by 
 * the Free Software Foundation, either version 3 of the License, or 
 * any later version. 
 * 
 * This program is distributed in the hope that it will be useful, 
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 * GNU General Public License for more details. 
 * 
 * You should have received a copy of the GNU General Public License 
 * along with this program under LICENSE-GNUGPL.  If not, see 
 * <http://www.gnu.org/licenses/>. 
 * 
 * 
 * All rights reserved. This program and the accompanying materials 
 * are made available under the terms of the Eclipse Public License v1.0 
 * which accompanies this distribution under LICENSE-ECLIPSE, and is 
 * available at http://www.eclipse.org/legal/epl-v10.html 
 */
package org.autorefactor.analyzer;

import static org.autorefactor.refactoring.ASTHelper.VISIT_SUBTREE;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import org.autorefactor.analyzer.PaaS.DetectionTypes;
import org.autorefactor.refactoring.ASTHelper;
import org.autorefactor.refactoring.rules.AbstractRefactoringRule;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jdt.core.dom.VariableDeclarationStatement;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/** See {@link #getDescription()} method. */
@SuppressWarnings("javadoc")
public class CRSAnalyzer extends AbstractRefactoringRule {

	CRSConsole crsConsole = new CRSConsole();
	PaaS paas;
	JSONObject json;
	String auxFileName = "";

	public CRSAnalyzer() {
		
		Company company = new GoogleCompany();
		this.paas = company.getPaaS();
//		this.paas = FactoryPaaS.getPaaS(PaaSModels.gae);
		this.paas.registerRestritedClasses();
		crsConsole.clear();
	}

	public boolean visit(FieldDeclaration node) {

		String fileName = ASTHelper.getFileName(node);
		if (!fileName.equals(this.auxFileName)) {
			json = new JSONObject();
		}
		if (this.paas.isPresentInRestritedClasses(ASTHelper.getQualifiedName(node))) {

			VariableDeclarationFragment fragment = (VariableDeclarationFragment) node.fragments().get(0);
			String variable = fragment.getName().toString();
			DetectionTypes currentDetection = DetectionTypes.declaration;
			List<String> listOfClasses = getClassesWithDetectionByType(currentDetection);
			this.showViolations(listOfClasses, node, currentDetection, ASTHelper.getQualifiedName(node),
					ASTHelper.getCLassName(node), variable);
			this.registerDetection(currentDetection, ASTHelper.getQualifiedName(node), "variable", variable);
			try {
				this.saveRegister(node);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		auxFileName = fileName;
		return VISIT_SUBTREE;
	}

	public boolean visit(VariableDeclarationStatement node) {

		String fileName = ASTHelper.getFileName(node);
		if (!fileName.equals(this.auxFileName)) {
			json = new JSONObject();
		}

		if (this.paas.isPresentInRestritedClasses(ASTHelper.getQualifiedName(node))) {

			VariableDeclarationFragment fragment = (VariableDeclarationFragment) node.fragments().get(0);
			String variable = fragment.getName().toString();

			DetectionTypes currentDetection = DetectionTypes.declaration;
			List<String> listOfClasses = getClassesWithDetectionByType(currentDetection);
			this.showViolations(listOfClasses, node, currentDetection, ASTHelper.getQualifiedName(node),
					ASTHelper.getCLassName(node), variable);

			this.registerDetection(currentDetection, ASTHelper.getQualifiedName(node), "variable", variable);

			try {
				this.saveRegister(node);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		auxFileName = fileName;
		return VISIT_SUBTREE;
	}

	private void registerDetection(DetectionTypes currentDetection, String qualifiedName, String variableType,
			String valueType) {

		if (json.get(currentDetection.toString()) != null) {

			JSONArray jsonArray = JSONAnalyzer.getValuesOfArray(json, currentDetection.toString(), variableType,
					qualifiedName);
			if (jsonArray != null) {
				JSONAnalyzer.addElementInArray(jsonArray, valueType);
			} else {
				JSONObject obj = JSONAnalyzer.getObjects(json, currentDetection.toString(), variableType);
				JSONAnalyzer.JSONCreateArrayInObject(obj, qualifiedName, valueType);
			}
		} else {

			JSONObject[] arrayJSONObjects = JSONAnalyzer.createObjects(json, currentDetection.toString(), variableType);
			JSONAnalyzer.JSONCreateArrayInObject(JSONAnalyzer.getSelf(arrayJSONObjects[1]), qualifiedName, valueType);

			// System.err.println(sempreAcreditar[0]);
			// JSONObject newJson = new JSONObject();
			// JSONObject newJson2 = new JSONObject();
			// JSONArray arrayVariable = new JSONArray();
			// arrayVariable.add(typeValue);
			// newJson2.put(qualifiedName, arrayVariable);
			// newJson.put(typeVariable, newJson2);
			// json.put(currentDetection.toString(), newJson);

		}
	}

//	public void addElementArray(JSONArray jsArray, Object object) {
//		if (!jsArray.contains(object)) {
//			jsArray.add(object);
//		}
//	}

	private void saveRegister(ASTNode node) throws IOException {
		String fileName = ASTHelper.getFileName(node);
		String fileToSave = System.getProperty("user.home") + "/crsanalyzer/" + fileName.replace(".java", ".json");
		File file = new File(fileToSave);
		file.getParentFile().mkdirs();
		file.createNewFile();

		FileWriter fileW = new FileWriter(fileToSave);
		fileW.write(json.toJSONString());
		fileW.flush();
		fileW.close();

		// System.err.println("Result"+json.toJSONString());

	}

	public boolean visit(ClassInstanceCreation node) {

		if (this.paas.isPresentInRestritedClasses(ASTHelper.getQualifiedName(node))) {
			DetectionTypes currentDetection = DetectionTypes.instanciation;
			List<String> listOfClasses = getClassesWithDetectionByType(currentDetection);
			this.showViolations(listOfClasses, node, currentDetection, ASTHelper.getQualifiedName(node),
					ASTHelper.getCLassName(node), null);
		}
		return VISIT_SUBTREE;
	}

	public boolean visit(TypeDeclaration node) {

		if (node.getSuperclassType() != null) {
			String qualifiedName = node.getSuperclassType().resolveBinding().getQualifiedName();
			String className = node.getSuperclassType().resolveBinding().getName();
			if (this.paas.isPresentInRestritedClasses(qualifiedName)) {
				DetectionTypes currentDetection = DetectionTypes.extension;
				List<String> listOfClasses = getClassesWithDetectionByType(currentDetection);
				this.showViolations(listOfClasses, node, currentDetection, qualifiedName, className, null);
			}
		}
		if (!node.superInterfaceTypes().toString().equals("[]")) {

			DetectionTypes currentDetection = DetectionTypes.implementation;
			List<String> listOfClasses = getClassesWithDetectionByType(currentDetection);

			Type itInterface = (Type) node.superInterfaceTypes().get(0);
			String qualifiedName = itInterface.resolveBinding().getQualifiedName();
			String className = itInterface.resolveBinding().getName();
			this.showViolations(listOfClasses, node, currentDetection, qualifiedName, className, null);

		}
		
		return VISIT_SUBTREE;
	}

	private void showViolations(List<String> listOfClasses, ASTNode node, DetectionTypes detectionType,
			String qualifiedName, String className, String variable) {
		for (String iteratedClass : listOfClasses) {
			if (iteratedClass.equals(qualifiedName)) {
				// showViolationByType(detectionType);
				showViolationByType2(detectionType, node, className, variable);
				showFile(node);
				showSolution(paas.abbreviation, className);
			}
		}
	}

	private void showSolution(String abbreviation, String className) {
		crsConsole.println("\tSolution: Use CRS4" + abbreviation + className + "Refactoring of CRSRefactor");
	}

	private void showViolationByType(DetectionTypes detectionType) {
		String violation = "";
		switch (detectionType) {
		case instanciation: {
			violation = "Instanciation ==> Can not instantiate restricted class";
			break;
		}
		case declaration: {
			violation = "Declaration ==> Can not declare constrained class variable";
			break;
		}
		case extension: {
			violation = "Extends ==> Can not extend restricted classes";
			break;
		}
		case implementation: {
			violation = "Implements ==> Can not implement interface of restricted classes";
			break;
		}
		}
		crsConsole.println("Violation: " + violation);
	}

	private void showViolationByType2(DetectionTypes detectionType, ASTNode node, String className, String variable) {

		String violation = "", description = "";
		String[] path = ASTHelper.getSourceLocationSeparated(node);

		switch (detectionType) {
		case instanciation: {
			violation = "Instanciation ==> Can not instantiate restricted class";
			description = "The restricted class \'" + className + "\' can not be instaciated";
			break;
		}
		case declaration: {
			violation = "Declaration ==> Can not declare constrained class variable";
			description = "The variable \'" + variable + "\' can not be defined by the restricted class \'" + className
					+ "\'";
			break;
		}
		case extension: {
			violation = "Extends ==> Can not extend restricted classes";
			description = "The class \'" + path[1].replace(".java", "") + "\' can not extends the restricted class \'"
					+ className + "\'";
			break;
		}
		case implementation: {
			violation = "Implements ==> Can not implement interface of restricted classes";
			description = "The class \'" + path[1].replace(".java", "")
					+ "\' can not implements the restricted interface \'" + className + "\'";
			break;
		}
		}
		crsConsole.println("Violation: " + violation + "\n\t" + "Description: " + description);
	}

	private void showFile(ASTNode node) {
		String[] path = ASTHelper.getSourceLocationSeparated(node);
		crsConsole.println(
				"\tFile: " + path[0] + "/" + path[1] + " at line number: " + path[2] + " and column: " + path[3]);
	}

	private List<String> getClassesWithDetectionByType(DetectionTypes typeDetection) {
		ArrayList<String> listOfClasses = new ArrayList<String>();
		for (Entry<String, List<DetectionTypes>> row : paas.getListRestrictedClasses().entrySet()) {
			for (DetectionTypes iteratedTypeDetection : row.getValue()) {
				if (iteratedTypeDetection.equals(typeDetection)) {
					listOfClasses.add(row.getKey());
				}
			}
		}
		return listOfClasses;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

}